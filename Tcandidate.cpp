/*
 * Tcandidate.cpp
 *
 *  Created on: 23 mar 2021
 *      Author: student
 */

#include <iostream>
#include <math.h>
#include <string>

#include "Tcandidate.h"

using namespace std;


////////////////////////////////////////////////
Tcandidate::Tcandidate()
{

	mark = 0;

	rand_gens_val();

}

////////////////////////////////////////////////

void Tcandidate::rand_gens_val()
{

	for (int i = 0; i < GENS_COUNT; i++)
	{
		genotype[i].set_rand_val();
	}

}

////////////////////////////////////////////////

void Tcandidate::rate()
{

	double x1 = genotype[0].get_val();
	double x2 = genotype[1].get_val();

	mark = pow((x1 + 3), 3) + x1*x2 + pow(x2, 2);

	if (mark) mark = 1/mark;

}

/////////////////////////////////////////////////

void Tcandidate::get_genotype_info()
{

      genotype[0].info();
      genotype[1].info();

}

////////////////////////////////////////

void Tcandidate::info()
{

	cout << "+++++++ Genotype ++++++\n";
	cout << "++ gens count: " << GENS_COUNT << "\n";

	for (int i = 0; i < GENS_COUNT; i++)
	{
		cout << "++ " << genotype[i].get_name() << ": " << genotype[i].get_val() << "\n";
	}

	cout << "++ \n";
	cout << "++ rate: " << mark << "\n";
	cout << "++++++++++++++++++++++++\n";

}

////////////////////////////////////////////////////////
