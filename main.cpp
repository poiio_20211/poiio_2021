#include <iostream>
#include <string>

#include "Tcandidate.h"
#include "Tgen.h"


using namespace std;


int main()
{
	Tcandidate can1 {};
	can1.rate(); ///////lab3
	can1.info();

	////////////////////////////
	//lab2
	Tgen gen1 {1,4,1,2,"Gen1","opis"};

	Tgen gen2 {0, 10, 0.5,"Gen2","opis"};
	gen2.set_val(2);

	gen1.info();
	gen2.info();

	cout << "modified vals: \n";

	gen1.set_val(5);

	gen1.set_description("Pierwszy gen");
	gen2.set_val(2.3);
	gen2.set_name("Gen_2.1");

	gen1.info();
	gen2.info();

	can1.get_genotype_info();

	return 0;
}


