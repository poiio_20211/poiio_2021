
#ifndef TGEN_H_
#define TGEN_H_


#include <string>


class Tgen
{

private:
	std::string name, description;
	double x_min, x_max, dx;
	unsigned int val_id;

public:
	Tgen();
    Tgen(double x_min, double x_max, double dx, std::string name, std::string description);
    Tgen(double x_min, double x_max, double dx, double val, std::string name, std::string description);
    Tgen(double x_min, double x_max, double dx, double val, std::string name);///lab3

    void set_name(std::string name) { this -> name = name; }
    std::string get_name() { return name; }

    void set_description(std::string des) { description = des ; }
    std::string get_description() { return description; }

    void set_range(double x_min, double x_max, double dx);

    //wartosc genu lab3
    void set_rand_val();

    void set_val(double val) { val_id = get_val_id(val); }
    double get_val() { return x_min + val_id * dx; }

    void info();

private:

    int get_val_id(double val);

};

#endif /* TGEN_H_ */
