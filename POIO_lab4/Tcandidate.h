/*
 * Tcandidate.h
 *
 *  Created on: 23 mar 2021
 *      Author: student
 */

#ifndef TCANDIDATE_H_
#define TCANDIDATE_H_



#include "Tgen.h"

#define GENS_COUNT 2



class Tcandidate
{

	Tgen genotype[GENS_COUNT] =
	{
			Tgen ( 0, 100, 1, 2, "x1"),
			Tgen ( 0, 100, 1, 2, "x2")
	};

	double mark;


public:

	Tcandidate();

	double get_mark() { return mark; };
	void rate();

	void info();

	void get_genotype_info();


private:

	void rand_gens_val();

 };


#endif /* TCANDIDATE_H_ */
