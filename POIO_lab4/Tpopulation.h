/*
 * Tpopulation.h
 *
 *  Created on: 25 mar 2021
 *      Author: student
 */

#ifndef TPOPULATION_H_
#define TPOPULATION_H_

#include <vector>
#include "Tcandidate.h"

class Tpopulation
{

	static unsigned int _id;
		   unsigned int candidates_count;

	std::vector <Tcandidate> candidates;
	double 					 best_val = 0;

public:

	Tpopulation(unsigned int cans_count = 0);
	void calculate();
	Tcandidate get_best_candidate();

	void info();

};



#endif /* TPOPULATION_H_ */
