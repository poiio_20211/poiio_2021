#include <iostream>
#include <math.h>
#include <string>

#include "Tgen.h"

using namespace std;
////////////////////////////////////lab3<
void Tgen::set_rand_val()
{

	int vals_count = fabs(x_max - x_min) / dx + 1;
	val_id = rand() % vals_count;
}

Tgen::Tgen(double x_min, double x_max, double dx, double val, std::string name)
{
	set_name(name);
	set_range(x_min, x_max, dx);
	set_val(val);
}
//////////////////////////////////// >lab3
Tgen::Tgen(double x_min, double x_max, double dx, double val, std::string name, std::string description)
{
	set_range(x_min, x_max, dx);
	set_val(val);
	set_name(name);
	set_description(description);
}


Tgen::Tgen(double x_min, double x_max, double dx, std::string name, std::string description)
{
	set_range(x_min, x_max, dx);
	set_name(name);
	set_description(description);
}



void Tgen::set_range(double x_min, double x_max, double dx)
{
	this -> x_min = x_min;
	this -> x_max = x_max;
	this -> dx = dx;
}


int Tgen::get_val_id(double val)
{
	if (val < x_min) return 0;

	else if (val > x_max) return (x_max - x_min) / dx;

	else
	{
		double x = x_min;
		unsigned int _id = 0;

		while ( fabs(x + _id * dx - val) > dx/2 ) _id++;

		val_id = _id;
	}
}




void Tgen::info()
{
	cout << "++++++++++++++++++++++++\n";
	cout << "++ Gen range \n";
	cout << "++ x_min: " << x_min << "\n";
	cout << "++ x_max: " << x_max << "\n";
	cout << "++ dx: " << dx << "\n";
	cout << "++ val_id: " << val_id <<"\n";
	cout << "++ value: " << get_val() <<"\n";
	cout << "++ nazwa: " << name << " - opis: " << description << "\n";
	cout << "++++++++++++++++++++++++\n\n";


}
