/*
 * Tpopulation.cpp
 *
 *  Created on: 25 mar 2021
 *      Author: student
 */

#include <iostream>
#include <algorithm>

#include "Tpopulation.h"

using namespace std;

//////////////////////////////////////////////////////////

unsigned int Tpopulation::_id = 0;

//////////////////////////////////////////////

Tpopulation::Tpopulation(unsigned int cans_count)
{

	_id++;
	candidates_count = cans_count;

	for (unsigned int i = 0; i < cans_count; i++)
	{
		candidates.push_back({});
	}

}

////////////////////////////////////////////////////////////

void Tpopulation::calculate()
{

	double best_val;

	for (int i = 0; i < candidates_count; i++)
	{

		candidates[i].rate();
		double val = candidates[i].get_mark();

		if (i) best_val = max(best_val, val);
		else   best_val = val;

	}

	this -> best_val = best_val;
}

//////////////////////////////////////////////////////////


Tcandidate Tpopulation::get_best_candidate()
{

	int i = 0;

	while (candidates[i].get_mark() !=best_val) i++;

	return candidates[i];
}

//////////////////////////////////////////////////////////

void Tpopulation::info()
{

	cout << "+++++ POPULATION #" << _id << " +++++\n";

	for (int i = 0; i < candidates_count; i++)
	{
		cout << "++ candidate#" << i << ": " << candidates[i].get_mark() << "\n";
		// candidates[i].info();
	}

	cout << "++++++++++++++++++++ \n";

}
